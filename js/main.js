var logo = document.getElementsByClassName('logo')[0].firstElementChild,
    title = document.getElementsByClassName('title')[0],
    bgimage = document.getElementsByClassName('background-image')[0],
    paragraph1 = document.getElementsByClassName('first')[0],
    paragraph2 = document.getElementsByClassName('second')[0],
    button = document.getElementsByTagName('button')[0];

//logo
TweenLite.to(logo, .5, {transform: 'scale(1)', ease:Power3.easeInOut});
TweenLite.to(logo, 1, {y: -40, ease:Power3.easeInOut}).delay(.4);

//title
TweenLite.to(title, 1, {y: 270, ease:Power3.easeInOut}).delay(.4);

//background-image
TweenLite.to(bgimage, .5, {autoAlpha: 1, ease:Power3.easeInOut}).delay(.8);
TweenLite.to(bgimage, 10, {x: -200, ease:Power1.easeInOut}).delay(1.3);
TweenLite.to(bgimage, 10, {x: 0, ease:Power1.easeInOut}).delay(11.3);

//paragraph
TweenLite.to(paragraph1, .5, {transform: 'scale(1)', ease:Power4.easeInOut}).delay(1.5);
TweenLite.to(paragraph2, .5, {transform: 'scale(1)', ease:Power4.easeInOut}).delay(1.7);

//button
TweenLite.to(button, .5, {y: -100, autoAlpha: 1, ease: Back.easeInOut.config(1.7)}).delay(2.2);

setTimeout(function(){
  banner.classList.add('hover-efect');
  var bannerHover = document.getElementsByClassName('hover-efect')[0];
  // bannerHover.addEventListener('mouseover', function(){
  //   console.log('eso');
  //   // TweenLite.to(button, .2, {padding: 25, ease: Power2.easeInOut});
  //   // TweenLite.to(button, .2, {padding: 25, ease: Power2.easeInOut});
  //   TweenLite.to(button, .2, {y: -100, autoAlpha: 1, transform: 'scale(1.1)', ease: Power2.easeInOut});
  //   TweenLite.to(button, .2, {y: -100, autoAlpha: 1, transform: 'scale(1)', ease: Power2.easeInOut}).delay(.2);
  //});
}, 3500);


// //title, logo, button
// TweenLite.to(logo, .9, {transform: 'scale(1)', ease:Power2.easeInOut});
// TweenLite.to(logo, 1, {y: -55}).delay(1);
// TweenLite.to(title, 1, {y: 225}).delay(1);
//
// //background-image
// TweenLite.to(bgimage, 1, {opacity: 1}).delay(1);
// TweenLite.to(bgimage, 10, {x: -100, ease:Power1.easeInOut}).delay(1);
// TweenLite.to(bgimage, 10, {x: 0, ease:Power1.easeInOut}).delay(11);
//
// //slogan
// TweenLite.to(slogan, .5, {y: 70}).delay(1.5)
// TweenLite.to(paragraph1, .5, {x: 355, ease:Power4.easeInOut}).delay(2);
// TweenLite.to(paragraph1, .5, {x: 1250, ease:Power4.easeInOut}).delay(5);
// TweenLite.to(paragraph2, .5, {x: 350, ease:Power4.easeInOut}).delay(5.2);
//
// //button
// TweenLite.to(button, .5, {transform: 'scale(1)', ease: Back.easeInOut.config(1.7)}).delay(2.5);
// TweenLite.to(button, .5, {transform: 'scale(1)', ease: Back.easeInOut.config(1.7)}).delay(2.5);
// //button animation
// setTimeout(function(){
//   banner.classList.add('hover-efect');
//   var bannerHover = document.getElementsByClassName('hover-efect')[0];
//   bannerHover.addEventListener('mouseover', function(){
//     TweenLite.to(button, .2, {transform: 'scale(1.1)', ease: Power2.easeInOut});
//     TweenLite.to(button, .2, {transform: 'scale(1)', ease: Power2.easeInOut}).delay(.2);
//   });
// }, 3500);
